module Gitlab
  module Homepage
    autoload :Team, 'lib/team'
    autoload :Event, 'lib/event'
    autoload :Competitor, 'lib/competitor'
    autoload :Category, 'lib/category'
    autoload :Stage, 'lib/stage'

    class Team
      autoload :Member, 'lib/team/member'
      autoload :Project, 'lib/team/project'
      autoload :Assignment, 'lib/team/assignment'
    end
  end
end
