---
layout: markdown_page
title: "Gartner Magic Quadrant for Application Release Orchestration 2018"
---

## GitLab and the Gartner Magic Quadrant for Application Release Orchestration 2018
This page represents how Gartner views our application release orchestration capabilities in relation to the larger market and how we're working with that information to build a better product. It also provides Gartner with ongoing context for how our product is developing in relation to how they see the market.

{::comment}
Jarek please add MQ graphic here similar to Forrester graphic.
{:/comment}

### Gartner's Key Takeaways on the ARO Market at time of report publication:

**Strategic Planning Assumption**
By 2023, 75% of global enterprises will have implemented at least one application release orchestration (ARO) solution, which is a substantial increase from fewer than 20% today.

**Market Definition/Description**
ARO tools provide a combination of deployment automation, pipeline and environment management, and release orchestration capabilities to simultaneously improve the quality, velocity and governance of application releases. ARO tools enable enterprises to scale release activities across multiple, diverse and multigenerational teams (e.g., DevOps), technologies, development methodologies (agile, etc.), delivery patterns (e.g., continuous), pipelines, processes and their supporting toolchains.

### Gartner's Take on GitLab at time of report publication:

**Strengths**
- The product’s low cost and heavy developer focus make it an effective option for getting started with CI/CD-oriented ARO.
- Product is cloud-native and open-source, with a large and rapidly growing customer base.
- It is a single product, and customers will have no confusion over what functionality they are purchasing.

**Cautions**
- A Git-intensive developer perspective on CI/CD pipelines and deployments may be of some concern for infrastructure and operations (I&O) leaders looking for more streamlined release, process-oriented or dashboard, and metrics-oriented operational functionality.
- Despite a broad set of product functionality, the breadth and depth of platform-specific support vary (e.g., it lacks native Microsoft OS support) and should be reviewed to determine fit.
- GitLabs’ addition of ARO-focused functionality (Auto DevOps) and positioning are relatively recent, uncharted territory for the company, raising the importance of consistent communication with the vendor and community to stay abreast of changes.

### Lessons Learned and Future Improvements

coming soon





